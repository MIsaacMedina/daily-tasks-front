import AboutView from './AboutView.vue';
import HomeView from './HomeView.vue';

export {
  AboutView,
  HomeView,
};